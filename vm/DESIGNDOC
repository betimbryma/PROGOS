       	    +---------------------------+
		    |         ProgOS		    |
		    | PROJECT 2: VIRTUAL MEMORY	|
		    |	   DESIGN DOCUMENT	    |
		    +---------------------------+

---- GROUP ----

>> Fill in the names and email addresses of your group members.

Betim Bryma <e1325255@student.tuwien.ac.at>
Raphael Gruber <raphi011@gmail.com>
Dardan Haxhimustafa <e1229164@student.tuwien.ac.at>

---- PRELIMINARIES ----

>> If you have any preliminary comments on your submission, notes for the
>> TAs, or extra credit, please give them here.

>> Please cite any offline or online sources you consulted while
>> preparing your submission, other than the Pintos documentation, course
>> text, lecture notes, and course staff.

			PAGE TABLE MANAGEMENT
			=====================
---- DATA STRUCTURES ----

>> A1: Copy here the declaration of each new or changed `struct' or
>> `struct' member, global or static variable, `typedef', or
>> enumeration.  Identify the purpose of each in 25 words or less.

enum page_type {
    FILE_T,
    ZERO_T
}; in Page.h we use these two nummerations to differentiate between the pages that we are goign to use for the stack growth and the ones that we use for VM. That means, we use ZERO_T for stack growth and FILE_T for Virtual Memory.

struct page {
	struct hash_elem hash_elem; /* Hash table element, like list_elem for example */
	struct hash_elem map_elem; /* We need it to save a page in our hash table of our mappings */
	void *addr; /*Virtual address */
	struct file *file; /* the file that is going to be be installed on the page, or already is */
	bool writable; /* whether this page is read-only or otherwise*/
	size_t page_read_bytes; /* the number of bytes that are going to be read */
	size_t page_zero_bytes; /* the number of bytes that are goign to be zero-ed*/
	off_t ofs; /* the offset of the file*/
	int ID; /* the ID of teh page that is going to be used for memory mapping*/
    enum page_type type; /* the type of the page that was explained above */
}; in Page.h we have created this structure that is going to represent one page for our system

struct hash pages - In struct thread, this is the supplementary page table for each thread

---- IMPLEMENTATION ----
>> A2: Describe how you implemented lazy page loading. How do you
>> determine the file and file offset corresponding to a virtual
>> address?

In the load segment function, instead of loading the page that was about to be installed, we save it on the supplementary page table of the current thread. We have the offset that we get as an argument from the load and then on each iteration we increace the offset by the number of the bytes that we have read. Then we have lazy page loading, because instead of loading that page in the load segment function, we have saved it in the supplementary page table, so when we have a page fault, we look for that virtual address on our supplementary page table, and if we find that page, we load it. That is per definition lazy loading.

---- SYNCHRONIZATION ----
>> A3: How do you deal with or prevent page faults in file system
>> routines. Argue why your solution is deadlock free.

As explained above, we use the page faults to acheive lazy loading, whenever there is a page fault we search for pages with that virtual address on the supplementary page table of the current thread, and if found, it gets loaded. Our solution is deadlock free, because we use the filse system lock in the load page function in the page.c whenever we are reading the file, and since that function only gets called in page_fault that means that that is the only process that can obtain that lock at that particular time. 

			    STACK GROWTH
			    ============

---- DATA STRUCTURES ----

>> B1: Copy here the declaration of each new or changed `struct' or
>> `struct' member, global or static variable, `typedef', or
>> enumeration.  Identify the purpose of each in 25 words or less.

The same ones that are used for VM, the ones that are explained above, are used here aswell.

---- IMPLEMENTATION ----
>> B2: Describe how you integrated the (potentially growing) stack segment
>> into your page table management implementation.

Once we have detected the stack growth request in the page_fault (it is explained below how we do that), we save it in our supplementary page table of the current thread, with the type ZERO_T, and load it, the page_loaod function we are able to differentiate between stack pages and other pages from the enummeration that was explained in the beginning. And whenever the type is ZERO_T we simply use the install_page function that does the rest of the work.


---- ALGORITHMS ----
>> B3: Explain how you detect a page fault which should trigger a
>> stack growth. What asssumptions are needed for your implementation
>> to work?

Whenever we have a page fault we look if the virtual address where the page fault occurred is smaller than the *esp of the interrupt frame, so with this heuristic we are able 'to guess' that a stack growth is desired, because the fault address is close to where the stack pointer is currently pointing at. Then we check to make sure that esp is currently pointing below PHYS_BASE - (8*1024*1024) because we don't allow the stack to grow past the 8 MB limit, because of efficiency issues. Then after that we create a new page in the supplementary page table of the current_thread and then we load that page.

			 MEMORY MAPPED FILES
			 ===================

---- DATA STRUCTURES ----

>> C1: Copy here the declaration of each new or changed `struct' or
>> `struct' member, global or static variable, `typedef', or
>> enumeration.  Identify the purpose of each in 25 words or less.

struct mmap_entry {
	struct hash_elem hash_elem; /* The hash_elem, because it is going to be saved in a hash table*/
	struct hash pages; /* the list of the pages in which this memory mapping, has mapped*/
	int entries; // To count the entries, that is pages, in which this memory mapping has mapped
	struct file *file; /*The file that this memory mapping is going to map*/
	int ID; /* each memory mapping has it's own unique IDs*/
}; - The struct is in Page.h and represents one memory mapping entry in the hash table of all the memory mapping that are done by the running thread, that is each thread saves it's own memory mappings in a list

struct hash mmaps - The table where we save the memory mappings done by the thread, to whom this table belongs to


---- ALGORITHMS ----

>> C2: Describe how memory mapped files integrate into your virtual
>> memory subsystem.

We make sure that all the necessary conitions are filled, such as if the file is bigger than zero, or if the file virtual address where this memory mapping is suppoused to happen is free and so on. Afterwars we create a mmap_entry with all the necessary informations and save all the pages that get created by reading the file/buffer in the list of the pages of the current mmap entry, but also on the supplementary page table of the thread, because we want to have a lazy memory mapping. 

>> C3: Explain how you determine whether a new file mapping overlaps
>> any existing segment.

We look up the supplementary page table of the current thread, looking if any of the pages that are saved have the same virtual address as the address where this memory mapping is supposed to happen. We also lookup the page directory of the thread and see if there are any pages that have the same virtual address as well. We do this in a for-loop by starting from the virtual memory address that we got in the parameter, but then increasing it by PGSIZE in each iteration.

---- RATIONALE ----

>> C4: Mappings created with "mmap" have similar semantics to those of
>> data demand-paged from executables, except that "mmap" mappings are
>> written back to their original files, not to swap.  This implies
>> that much of their implementation can be shared.  Explain why your
>> implementation either does or does not share much of the code for
>> the two situations.

			   SURVEY QUESTIONS
			   ================

Answering these questions is optional, but it will help us improve the
course in future quarters.  Feel free to tell us anything you
want--these questions are just to spur your thoughts.  You may also
choose to respond anonymously in the course evaluations at the end of
the quarter.

>> In your opinion, was this assignment, or any one of the three problems
>> in it, too easy or too hard?  Did it take too long or too little time?

>> Did you find that working on a particular part of the assignment gave
>> you greater insight into some aspect of OS design?
To fully understand how to implement memory mapping was a little tricky, but totally worth it in the end because nwo we understand how the system calls work.

>> Is there some particular fact or hint we should give students in
>> future quarters to help them solve the problems?  Conversely, did you
>> find any of our guidance to be misleading?

>> Do you have any suggestions for the TAs to more effectively assist
>> students, either for future quarters or the remaining projects?
Personally (Betim) I think that the analys talks should be a little later, because it is impossible to discuss an implementation plan with the tutor by having it so early. I believe that it would be more helpful to the future generations if it was a little later, or at least two meetings with the tutor per project.

>> Any other comments?
We enjoyed taking you class, and wish you all the best! 